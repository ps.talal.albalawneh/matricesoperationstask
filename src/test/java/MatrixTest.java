import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MatrixTest {

    @Test
    void givenNullArray_whenSetDataOrInitializeMatrixObject_thenExceptionThrown() {
        double[][] data = new double[3][3];
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix matrix = new Matrix(null);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix matrix = new Matrix(data);
            matrix.setData(null);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Data Array should NOT be null.", exception.getMessage());
    }

    @Test
    void givenInvalidDataArrayWithNullRowOrMissingCol_whenSetDataOrInitializeMatrixObject_thenExceptionThrown() {
        double[][] data = new double[3][3];
        double[][] nullRowData = {
                {1, 2, 3},
                null,
                {1, 2, 3}};
        double[][] missingColData = {
                {1, 2, 3},
                {1, 2},
                {1, 2, 3}};
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix matrix = new Matrix(nullRowData);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix matrix = new Matrix(data);
            matrix.setData(nullRowData);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Data should NOT have null row.", exception.getMessage());

        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix matrix = new Matrix(missingColData);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix matrix = new Matrix(data);
            matrix.setData(missingColData);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Data should NOT have null col.", exception.getMessage());
    }

    @Test
    void givenNullMatrix_whenSum_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        Matrix nullMatrix = null;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.sum(nullMatrix);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenFirstNotNullMatrixAndSecondNullMatrix_whenSum_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        Matrix nullMatrix = null;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.sumOfMatrices(matrix, nullMatrix);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenFirstNullMatrixAndSecondNotNullMatrix_whenSum_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        Matrix nullMatrix = null;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.sumOfMatrices(nullMatrix, matrix);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenFirstNullMatrixAndSecondNullMatrix_whenSum_thenExceptionThrown() {
        Matrix nullMatrix = null;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.sumOfMatrices(nullMatrix, nullMatrix);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenFirstNotNullMatrixAndSecondNotNullMatrix_whenSum_thenResultIsReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        Matrix expected = new Matrix(new double[][]{
                {2, 4, 6},
                {2, 4, 6},
                {2, 4, 6}});
        double[][] actual = Matrix.sumOfMatrices(matrix, matrix);

        Assertions.assertArrayEquals(expected.getData(), actual);
    }

    @Test
    void givenNullMatrix_whenScalarMultiplication_thenExceptionThrown() {
        Matrix nullMatrix = null;

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.scalarMultiplication(nullMatrix, 5);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenNotNullMatrix_whenScalarMultiplication_thenResultReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        double[][] expected = {
                {2, 4, 6},
                {2, 4, 6},
                {2, 4, 6}};
        double[][] actual = Matrix.scalarMultiplication(matrix, 2);
        Assertions.assertArrayEquals(expected, actual);
        matrix.scalarMultiplication(2);
        Assertions.assertArrayEquals(expected, matrix.getData());
    }

    @Test
    void givenNullMatrix_whenTransport_thenExceptionThrown() {
        Matrix nullMatrix = null;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.transport(nullMatrix);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenMatrix_whenTransport_thenResultReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        double[][] expected = {
                {1, 1, 1},
                {2, 2, 2},
                {3, 3, 3}};
        double[][] actual = Matrix.transport(matrix);
        Assertions.assertArrayEquals(expected, actual);

        matrix.transport();
        Assertions.assertArrayEquals(expected, matrix.getData());
    }

    @Test
    void givenFirstNotNullMatrixAndSecondNullMatrix_whenMultiply_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        Matrix nullMatrix = null;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.multiply(matrix, nullMatrix);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenFirstNullMatrixAndSecondNotNullMatrix_whenMultiply_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        Matrix nullMatrix = null;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.multiply(nullMatrix, matrix);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenFirstNullMatrixAndSecondNullMatrix_whenMultiply_thenExceptionThrown() {
        Matrix nullMatrix = null;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.sumOfMatrices(nullMatrix, nullMatrix);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Null matrix -> you should initialize the matrix.", exception.getMessage());
    }

    @Test
    void givenFirstNotNullMatrixAndSecondNotNullMatrix_whenMultiply_thenResultIsReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        double[][] expected = {
                {6, 12, 18},
                {6, 12, 18},
                {6, 12, 18}};
        double[][] actual = Matrix.multiply(matrix, matrix);

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void givenMatrixAndPassesNotNullMatrix_whenMultiply_thenResultIsReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        double[][] expected = {
                {6, 12, 18},
                {6, 12, 18},
                {6, 12, 18}};
        matrix.multiply(matrix);

        Assertions.assertArrayEquals(expected, matrix.getData());
    }

    @Test
    void givenTwoInvalidToMultiplyMatrices_whenMultiply_thenExceptionThrown() {
        Matrix matrix1 = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        Matrix matrix2 = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3}});
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Matrix.multiply(matrix1, matrix2);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("Those matrices can NOT be multiply.", exception.getMessage());
    }

    @Test
    void givenInvalidRowAndValidCol_whenRemoveRowCol_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.removeRowCol(-1, 1);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The row must be grater than zero.", exception.getMessage());
    }

    @Test
    void givenValidRowAndInvalidCol_whenRemoveRowCol_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.removeRowCol(1, -1);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The col must be grater than zero.", exception.getMessage());
    }

    @Test
    void givenOutOfBoundRowAndValidCol_whenRemoveRowCol_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        int row = 4;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.removeRowCol(row, 1);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The row #" + row + " is grater than number of rows.", exception.getMessage());
    }

    @Test
    void givenValidRowAndOutOfBoundCol_whenRemoveRowCol_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        int col = 4;
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.removeRowCol(1, col);
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("The col #" + col + " is grater than number of cols.", exception.getMessage());
    }

    @Test
    void givenValidRowAndValidCol_whenRemoveRowCol_thenResultReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});
        double[][] expected = {
                {2, 3},
                {2, 3}};
        double[][] actual = matrix.removeRowCol(3, 1);

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void givenNonSquareMatrix_whenChangeToDiagonal_thenExceptionThrown() {

        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3}});
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.changeToDiagonal();
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("This operation is made for a square matrices where number of columns is equal to the number of rows.", exception.getMessage());
    }

    @Test
    void givenSquareMatrix_whenChangeToDiagonal_thenResultIsReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});

        double[][] expected = {
                {1, 0, 0},
                {0, 2, 0},
                {0, 0, 3}};
        matrix.changeToDiagonal();
        double[][] actual = matrix.getData();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void givenNonSquareMatrix_whenChangeToLowerTriangular_thenExceptionThrown() {

        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3}});
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.changeToLowerTriangular();
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("This operation is made for a square matrices where number of columns is equal to the number of rows.", exception.getMessage());
    }

    @Test
    void givenSquareMatrix_whenChangeToLowerTriangular_thenResultIsReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});

        double[][] expected = {
                {1, 0, 0},
                {1, 2, 0},
                {1, 2, 3}};
        matrix.changeToLowerTriangular();
        double[][] actual = matrix.getData();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void givenNonSquareMatrix_whenChangeToUpperTriangular_thenExceptionThrown() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3}});
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.changeToUpperTriangular();
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("This operation is made for a square matrices where number of columns is equal to the number of rows.", exception.getMessage());
    }

    @Test
    void givenSquareMatrix_whenChangeToUpperTriangular_thenResultIsReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});

        double[][] expected = {
                {1, 2, 3},
                {0, 2, 3},
                {0, 0, 3}};
        matrix.changeToUpperTriangular();
        double[][] actual = matrix.getData();
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void givenNonSquareMatrix_whenDeterminant_thenExceptionThrown(){
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3}});
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            matrix.changeToUpperTriangular();
        });
        Assertions.assertEquals(IllegalArgumentException.class, exception.getClass());
        Assertions.assertEquals("This operation is made for a square matrices where number of columns is equal to the number of rows.", exception.getMessage());
    }

    @Test
    void givenSquareMatrix_whenDeterminant_thenResultIsReturned() {
        Matrix matrix = new Matrix(new double[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}});

        double expected = 0;
        double actual = matrix.determinant();

        Assertions.assertEquals(expected, actual);
    }
}