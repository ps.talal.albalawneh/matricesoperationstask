
public class Matrix extends MatrixUtility {
    private double[][] data;
    private int rows;
    private int cols;

    public Matrix(double[][] data) {
        MatrixValidation.checkValidData(data);
        this.data = data;
        this.rows = data.length;
        this.cols = data[0].length;
    }

    public void setData(double[][] data) {
        MatrixValidation.checkValidData(data);
        this.data = data;
        this.rows = data.length;
        this.cols = data[0].length;
    }

    public double[][] getData() {
        return data;
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public double[][] sum(Matrix matrix) {
        MatrixValidation.checkValidMatricesSum(this, matrix);
        return sumOfMatrices(this, matrix);
    }

    public void scalarMultiplication(double scalar) {
        this.setData(scalarMultiplication(this, scalar));
    }

    public void transport() {
        this.setData(transport(this));
    }

    public void multiply(Matrix matrix) {
        MatrixValidation.checkValidMatricesMultiplication(this, matrix);
        this.setData(multiply(this, matrix));
    }

    public double[][] removeRowCol(int row, int col) {
        MatrixValidation.validateRemoveRowCol(this, row, col);
        double[][] data = this.getData();
        if (col != 0) {
            data = removeCol(col);
        }
        if (row != 0) {
            data = removeRow(row);
        }
        return data;
    }

    public void changeToDiagonal() {
        MatrixValidation.isSquareMatrix(this);
        changeValueDirection(ValuesDirection.DIAGONAL);
    }

    public void changeToLowerTriangular() {
        MatrixValidation.isSquareMatrix(this);
        changeValueDirection(ValuesDirection.LOWER_TRIANGULAR);
    }

    public void changeToUpperTriangular() {
        MatrixValidation.isSquareMatrix(this);
        changeValueDirection(ValuesDirection.UPPER_TRIANGULAR);
    }

    public double determinant() {
        MatrixValidation.isSquareMatrix(this);
        return this.recDeterminant(this.getData());
    }

    private enum ValuesDirection {
        DIAGONAL, UPPER_TRIANGULAR, LOWER_TRIANGULAR;

        static boolean getCondition(ValuesDirection direction, int row, int col) {
            if (direction == null)
                throw new IllegalArgumentException("Null direction can't be resolve, select your direction.");
            if (direction == UPPER_TRIANGULAR) return row <= col;
            if (direction == LOWER_TRIANGULAR) return row >= col;
            return row == col;
        }
    }

    private void changeValueDirection(ValuesDirection direction) {
        double[][] data = this.getData();
        int rows = this.getRows();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                if (ValuesDirection.getCondition(direction, i, j))
                    continue;
                data[i][j] = 0;
            }
        }
        this.setData(data);
    }

    private double[][] removeCol(int col) {
        double[][] data = this.getData();
        double[][] res = new double[data.length][data[0].length - 1];
        int r = 0;
        for (int i = 0; i < data.length; i++) {
            int c = 0;
            for (int j = 0; j < data[0].length; j++) {
                if (j == col - 1)
                    continue;
                res[r][c++] = data[i][j];
            }
            r++;
        }
        this.setData(res);
        return res;
    }

    private double[][] removeRow(int row) {
        double[][] data = this.getData();
        double[][] res = new double[data.length - 1][data[0].length];
        int r = 0;
        for (int i = 0; i < data.length; i++) {
            int c = 0;
            if (i == row - 1)
                continue;
            for (int j = 0; j < data[0].length; j++) {
                res[r][c++] = data[i][j];
            }
            r++;
        }
        this.setData(res);
        return res;
    }

    private double recDeterminant(double[][] data) {
        double dt = 0;
        if (data.length > 2) {
            for (int i = 0; i < data.length; i++) {
                dt += ((i % 2 == 0) ? 1 : -1) * data[0][i] * recDeterminant(this.subMatrixIgnoreRowCol(data, 1, i + 1));
            }
        } else {
            dt = data[0][0] * data[1][1] - data[0][1] * data[1][0];
        }
        return dt;
    }

    private double[][] subMatrixIgnoreRowCol(double[][] data, int row, int col) {
        double[][] res = new double[data.length - 1][data[0].length - 1];
        int r = 0;
        for (int i = 0; i < data.length; i++) {
            int c = 0;
            if (i == row - 1)
                continue;
            for (int j = 0; j < data[0].length; j++) {
                if (j == col - 1)
                    continue;
                res[r][c++] = data[i][j];
            }
            r++;
        }
        return res;
    }

}
