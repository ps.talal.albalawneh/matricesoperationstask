public class MatrixValidation {

    public static void checkValidMatricesSum(Matrix matrix1, Matrix matrix2) {
        checkIfNull(matrix1, matrix2);
        if (matrix1.getRows() != matrix2.getRows()) {
            throw new IllegalArgumentException("Number of rows are NOT the same.");
        }
        if (matrix1.getCols() != matrix2.getCols()) {
            throw new IllegalArgumentException("Number of cols are NOT the same.");
        }
    }

    public static void checkIfNull(Matrix... matrices) {
        for (Matrix matrix : matrices) {
            if (matrix == null)
                throw new IllegalArgumentException("Null matrix -> you should initialize the matrix.");
        }
    }

    public static void checkValidMatricesMultiplication(Matrix matrix1, Matrix matrix2) {
        checkIfNull(matrix1, matrix2);
        if (matrix1.getCols() != matrix2.getRows()) {
            throw new IllegalArgumentException("Those matrices can NOT be multiply.");
        }
    }

    public static void checkValidRowColData(double[][] data) {
        for (int i = 0; i < data.length; i++) {
            if (data[i] == null)
                throw new IllegalArgumentException("Data should NOT have null row.");
            if (i > 0) {
                if (data[i - 1].length != data[i].length)
                    throw new IllegalArgumentException("Data should NOT have null col.");
            }
        }
    }

    public static void checkValidData(double[][] data) {
        if (data == null) {
            throw new IllegalArgumentException("Data Array should NOT be null.");
        }
        checkValidRowColData(data);
    }

    public static void isSquareMatrix(Matrix matrix) {
        if (matrix.getRows() != matrix.getCols()) {
            throw new IllegalArgumentException("This operation is made for a square matrices where number of columns is equal to the number of rows.");
        }
    }

    public static void validateRemoveRowCol(Matrix matrix, int row, int col) {
        if (col < 0) {
            throw new IllegalArgumentException("The col must be grater than zero.");
        }
        if (col > matrix.getCols()) {
            throw new IllegalArgumentException("The col #" + col + " is grater than number of cols.");
        }
        if (row < 0) {
            throw new IllegalArgumentException("The row must be grater than zero.");
        }
        if (row > matrix.getRows()) {
            throw new IllegalArgumentException("The row #" + row + " is grater than number of rows.");
        }
    }

}