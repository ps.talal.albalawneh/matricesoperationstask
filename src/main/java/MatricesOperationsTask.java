public class MatricesOperationsTask {

    public static void main(String[] args){
        double[][] data = new double[][]{{6,1,1},{4,-2,5},{2,8,7}};
        double[][] data1 = new double[][]{{1,2,3,4},{1,2,3,4}};

        Matrix matrix1 = new Matrix(data);
        double[][] res = Matrix.scalarMultiplication(matrix1, 2);
        double[][] dd = matrix1.getData();
        for (int i = 0; i < matrix1.getRows(); i++) {
            for (int j = 0; j < matrix1.getCols(); j++) {
                System.out.print(res[i][j]+" ");
            }
            System.out.println();
        }
    }
}
