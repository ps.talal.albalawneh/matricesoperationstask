public class MatrixUtility {

    public static double[][] sumOfMatrices(Matrix matrix1, Matrix matrix2) {
        MatrixValidation.checkValidMatricesSum(matrix1, matrix2);
        return doSum(matrix1, matrix2);
    }

    public static double[][] scalarMultiplication(Matrix matrix, double scalar) {
        MatrixValidation.checkIfNull(matrix);
        return doScale(matrix, scalar);
    }

    public static double[][] transport(Matrix matrix) {
        MatrixValidation.checkIfNull(matrix);
        return doTransport(matrix);
    }

    public static double[][] multiply(Matrix matrix1, Matrix matrix2) {
        MatrixValidation.checkValidMatricesMultiplication(matrix1, matrix2);
        return doMultiplication(matrix1, matrix2);
    }

    private static double[][] doSum(Matrix matrix1, Matrix matrix2) {
        int rows = matrix1.getRows();
        int cols = matrix1.getCols();
        double[][] matrix1Data = matrix1.getData();
        double[][] matrix2Data = matrix2.getData();
        double[][] result = new double[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                result[i][j] = matrix1Data[i][j] + matrix2Data[i][j];
            }
        }
        return result;
    }

    private static double[][] doScale(Matrix matrix, double scalar) {
        double[][] data = matrix.getData();
        int rows = matrix.getRows();
        int cols = matrix.getCols();
        double[][] scaledData = new double[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                scaledData[i][j] = data[i][j] * scalar;
            }
        }
        return scaledData;
    }

    private static double[][] doTransport(Matrix matrix) {
        double[][] data = matrix.getData();
        int rows = matrix.getRows();
        int cols = matrix.getCols();
        double[][] res = new double[cols][rows];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                res[j][i] = data[i][j];
            }
        }
        return res;
    }

    private static double[][] doMultiplication(Matrix matrix1, Matrix matrix2) {
        double[][] matrix1Data = matrix1.getData();
        double[][] matrix2Data = matrix2.getData();
        int newRows = matrix1.getRows();
        int newCols = matrix2.getCols();
        double[][] res = new double[newRows][newCols];
        for (int i = 0; i < newRows; i++) {
            for (int j = 0; j < newCols; j++) {
                int sum = 0;
                for (int k = 0; k < matrix1.getCols(); k++) {
                    sum += matrix1Data[i][k] * matrix2Data[k][j];
                }
                res[i][j] = sum;
            }
        }
        return res;
    }

}
